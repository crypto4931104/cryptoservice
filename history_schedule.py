import schedule
import subprocess
import time


def job() -> None:
    """
    Run the external job responsible for adding fresh cryptocurrency data periodically to the application database.

    :return: None
    """

    # Running the job as an external process is meant to minimize the risk of memory leaks.
    subprocess.run(["python", "history_push.py"])


# The time resolution is dictated by that of the external APIs.
schedule.every(1).minutes.do(job)

while 1:
    schedule.run_pending()
    time.sleep(1)
