import datetime
import json
import motor.motor_asyncio
import os
import pymongo.collection
import redis.asyncio as redis
from fastapi import FastAPI, Query
from fastapi.responses import JSONResponse, PlainTextResponse
from urllib.parse import quote_plus

import crypto_utils
from integration_client import IntegrationRpcClient


RABBITMQ_URL = os.environ["RABBITMQ_URL"]
RPC_INTEGRATION_QUEUE = os.environ["RPC_INTEGRATION_QUEUE"]
REDIS_HOST = os.environ["REDIS_HOST"]
REDIS_CACHE_EXPIRATION = int(os.environ["REDIS_CACHE_EXPIRATION"])
MONGODB_HOST = os.environ["MONGODB_HOST"]
MONGODB_USER = os.environ["MONGODB_USER"]
MONGODB_PWD = os.environ["MONGODB_PWD"]
MONGODB_DBNAME = os.environ["MONGODB_DBNAME"]
MONGODB_COLLECTION = os.environ["MONGODB_COLLECTION"]

MONGODB_URI = f"mongodb://{quote_plus(MONGODB_USER)}:{quote_plus(MONGODB_PWD)}@{MONGODB_HOST}/{MONGODB_DBNAME}?authSource=admin"

integration_client: IntegrationRpcClient
historical_collection: pymongo.collection.Collection
redis_client: redis.Redis

app = FastAPI()


@app.on_event("startup")
async def startup() -> None:
    """
    Establish connections to the data sources in the backend used by the application's endpoint.

    :return: None
    """
    global integration_client
    global historical_collection
    global redis_client
    # Establish connection to RabbitMQ using custom async RPC client.
    integration_client = await IntegrationRpcClient(RABBITMQ_URL, RPC_INTEGRATION_QUEUE).connect()
    # Establish connection to MongoDB and initialize cursor to target collection.
    # mongo_client = MongoClient(MONGODB_URI)
    mongo_client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URI)
    db = mongo_client[MONGODB_DBNAME]
    historical_collection = db[MONGODB_COLLECTION]
    # Establish connection to Redis cache using async client.
    redis_client = redis.Redis(host=REDIS_HOST, port=6379, decode_responses=True)


@app.get("/")
async def price_list(limit: int = Query(100, ge=0),
                     timestamp: datetime.datetime | str = Query("NOW", alias="datetime"),
                     output_format: str = Query("csv", alias="format", pattern="^(json|csv)$")):
    """
    Application's endpoint serving cryptocurrency data extracted from Coinmarketcap (price in USD) and
     Cryptocompare (ranking by 24h Full Volume).

    :param limit: A non-negative integer indicating the maximum number of cryptocurrency records to return.
    Defaults to 100. If zero, all available cryptocurrency records are returned.
    :param timestamp: The string NOW (default), or a UTC timestamp in ISO 8601 (clients should mind URL encoding).
    If NOW, freshly-retrieved data from calls to the external APIs are returned.
    If a valid timestamp is provided, data from the historical database for that timestamp will be returned if available.
    Timestamps are truncated to one-minute granularity (as dictated by the time resolution of the external APIs).
    :param output_format: A string describing the desired output format, either csv (default) or json.
    :return: A PlainTextResponse/JSONResponse from a list of retrieved cryptocurrency records, by ascending rank.
    """

    if timestamp == "NOW":
        # By default, request a fresh batch of cryptocurrency data by performing an asynchronous
        # RPC to the integration service, which will perform calls to the pricing and ranking services and return
        # integrated price and ranking information.
        data_str = await integration_client.call()
        if data_str is None:
            return PlainTextResponse(content="Retrieval failed", status_code=500)

        # Obtain a UTC timestamp key (truncated to minutes) and cache the results from the integration service.
        timestamp_key = datetime.datetime.now(tz=datetime.UTC).strftime("%y/%m/%d-%H:%M")
        await redis_client.set(timestamp_key, data_str, ex=REDIS_CACHE_EXPIRATION)

        data = json.loads(data_str)
    else:
        # The user provided a custom datetime string to retrieve data from historical results.
        try:
            timestamp = datetime.datetime.fromisoformat(timestamp)
        except ValueError:
            return PlainTextResponse(content=f"Unrecognized datetime format {timestamp}, use ISO 8601 and mind URL encoding.",
                                     status_code=500)

        # Timestamps must be UTC for compatibility with those used by the history service.
        if timestamp.tzinfo is not datetime.UTC:
            return PlainTextResponse(
                content=f"Timestamp '{timestamp}' is not UTC.",
                status_code=500)

        # The timestamp provided by the user is valid.
        # Perform a cache/database lookup at one-minute resolution.
        timestamp = timestamp.replace(second=0, microsecond=0)
        timestamp_key = timestamp.strftime("%y/%m/%d-%H:%M")

        # Look up for cached (Redis) results first.
        data_str: str = await redis_client.get(timestamp_key)
        if data_str is not None:
            data = json.loads(data_str)
        else:
            # The timestamp is not cached; perform a historical database (MongoDB) lookup.
            result = await historical_collection.find_one({"timestamp": {"$eq": timestamp}})
            if result is not None:
                data = result["data"]
                # Cache the results from the historical database lookup.
                await redis_client.set(timestamp_key, json.dumps(data), ex=REDIS_CACHE_EXPIRATION)
            else:
                return PlainTextResponse(content=f"No historical record available for timestamp {timestamp_key}",
                                         status_code=500)

    # Apply the limit parameter (data are sorted by ascending ranking).
    data = crypto_utils.apply_limit(data, limit=limit)

    # Return a response in the desired format.
    if output_format == 'csv':
        return PlainTextResponse(content=crypto_utils.as_csv(data), status_code=200)
    else:
        return JSONResponse(data, status_code=200)
