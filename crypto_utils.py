def simplify_coinmarketcap_item(crypto_item: dict) -> dict:
    """
    Simplify a coinmarketcap cryptocurrency record to a dictionary containing only a symbol and a price.

    :param crypto_item: A cryptocurrency item from a Coinmarketcap API call.
    :return: A dictionary containing the cryptocurrency's symbol and its price in USD.
    """
    return {'symbol': crypto_item['symbol'], 'price': crypto_item['quote']['USD']['price']}


def integrate_price_ranking(data_price: list[dict], data_ranking: dict) -> list[dict]:
    """
    Integrate cryptocurrency price (USD) information from Coinmarketcap with ranking information (24h Full Volume)
    from Cryptocompare. Only records for cryptocurrency symbols for which both price and ranking data are available
    are returned.

    :param data_price: A list of dictionaries representing cryptocurrency results and containing a symbol and a price.
    :param data_ranking: A dictionary containing ranking information indexed by cryptocurrency symbol.
    :return: A list of dictionary records containing symbol, price and ranking, sorted by ascending ranking.
    """
    # Assign ranking data to price items.
    data_integrated = data_price[:]

    for item in data_integrated:
        rank = None
        if item['symbol'] in data_ranking:
            rank = data_ranking[item['symbol']]
        item["rank"] = rank

    # Remove items with missing rank information and return list sorted by rank (descending volume).
    data_integrated = list(filter(lambda x: x["rank"] is not None, data_integrated))
    data_integrated = sorted(data_integrated, key=lambda x: int(x["rank"]), reverse=False)
    return data_integrated


def apply_limit(data: list, limit: int = 0) -> list:
    """
    Slice a list to a maximum length.

    :param data: A list to be sliced.
    :param limit: The maximum length of the output list. If zero, no slicing takes place.
    :return: A new list of limited length.
    """
    # Apply limit parameter (expects data sorted by ascending ranking).
    if limit and len(data) > limit:
        data = data[:limit]
    return data


def as_csv(assets: list[dict]) -> str:
    """
    Convert a list of cryptocurrency records to csv output.

    :param assets: A list of cryptocurrency records containing rank, symbol and price.
    :return: A csv-formatted multiline string including a header.
    """

    rows = [",".join(["Rank", "Symbol", "Price USD"])]
    for item in assets:
        row_values = [str(item[field]) for field in ["rank", "symbol", "price"]]
        rows.append(",".join(row_values))
    return "\n".join(rows)
