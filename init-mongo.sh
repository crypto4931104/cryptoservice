#!/bin/bash
set -e

mongosh <<EOF
use admin
db.createUser(
  {
    user: "$MONGODB_USER",
    pwd: "$MONGODB_PWD",
    roles: [ { role: "readWrite", db: "$MONGODB_DBNAME" } ]
  }
)

db = new Mongo().getDB("$MONGODB_DBNAME");
db.createCollection("$MONGODB_COLLECTION")
EOF