import json
from fastapi import FastAPI, Query
from fastapi.responses import PlainTextResponse

import crypto_queries
import crypto_utils


app = FastAPI()


@app.get("/")
def price_list(limit: int = 100, output_format: str | None = Query('json', alias="format")):
    """
    An endpoint implementing a naïve solution based on sequential calls to external APIs and
    with limited functionality.

    :param limit: The maximum number cryptocurrency of records to return.
    :param output_format: The desired output format (json or csv).
    :return: Cryptocurrency records in the desired format.
    """

    data = crypto_queries.fetch_price_ranking(limit=limit)

    if data is None:
        pass  # return appropriate response

    if output_format == 'csv':
        return PlainTextResponse(content=crypto_utils.as_csv(data), status_code=200)

    return json.dumps(data)
