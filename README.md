



# Cryptoservice

### Trade your day

&nbsp;

This application provides a simple API to retrieve cryptocurrency price and volume ranking information from third-party
services and return integrated, properly formatted output. Both up-to-date and historical information can be retrieved.

&nbsp;

#### Deployment

The application is made up by a number of interacting microservices and is best deployed using docker compose.

```shell
cd <path_to_app>
docker compose up -d
```

You will need to set a number environment variables. The following ones are
dictated by the current architecture of the application and should not be modified.  

```
    RABBITMQ_URL: amqp://guest:guest@rabbitmq
    PRICING_SERVICE_URL: http://pricing:5001
    RANKING_SERVICE_URL: http://ranking:5002
    MONGODB_HOST: mongodb:27017
    REDIS_HOST: redis
    COINMARKETCAP_URL: https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest
    CRYPTOCOMPARE_URL: https://min-api.cryptocompare.com/data/top/totalvolfull
```

The following can be customized each time the app is deployed afresh:

```
    REDIS_CACHE_EXPIRATION: 300
    RPC_INTEGRATION_QUEUE: rpc_integration_queue
    MONGODB_USER: historical_user
    MONGODB_PWD: <whatever_suits_you>
    MONGODB_DBNAME: historical
    MONGODB_COLLECTION: historical_data
```

You will need to provide your own values for the following:

```
    COINMARKETCAP_KEY   # Your key to access the Coinmarketcap API (do not use a sandbox key).
    CRYPTOCOMPARE_KEY   # Your key to access the Cryptocompare API.
    MONGO_INITDB_ROOT_USERNAME
    MONGO_INITDB_ROOT_PASSWORD
```

&nbsp;

#### Usage

The HTTP endpoint uses port 6667. An example call looks like this:

```shell
curl "http://localhost:6667/"
```

Cryptocurrency records are always returned by ascending volume rank.
By default, the application returns a maximum of 100 csv-formatted records.
It is possible to change these settings limit using the `limit` and `format` query parameters. 
The call above is equivalent to:

```shell
curl "http://localhost:6667?limit=100&format=csv"
```

For example, to return a maximum of 10 records, use:

```shell
curl "http://localhost:6667?limit=10"
```

As for the return format, it is also possible to obtain a JSON response:

```shell
curl "http://localhost:6667?limit=4&format=json"
```

Additionally, retrieval of historical records by timestamp is also
possible using the `datetime` query parameter. Accepted values are URL-encoded, ISO 8601-compliant UTC datetimes.
For example:

```shell
curl "http://localhost:6667?limit=5&datetime=2024-01-23T22%3A54%3A11.106405%2B00%3A00"
```

You can easily provide compatible timestamps using Python:

````python
import datetime
from urllib.parse import quote_plus

quote_plus(datetime.datetime.now(tz=datetime.UTC).isoformat())
````

Alternatively, you can use NOW to force the application to return the most recent data available from 
third-party services, bypassing the cache. This is the default behavior when no timestamp is provided.

```shell
curl "http://localhost:6667?limit=5&datetime=NOW"
```

&nbsp;


#### Architecture.

The application consists of an HTTP endpoint, a pricing service, a ranking service,
an integration service, a history service, a historical records database, a message broker
and a cache.

Upon receiving a request, the endpoint performs a remote procedure call (via RabbitMQ) to the integration
service, which in turn performs HTTP requests on the pricing and ranking services, integrates
price and ranking information, filters out incomplete cryptocurrency records and sends the 
results back to the endpoint.

The pricing service obtains up-to-date cryptocurrency prices by performing HTTP requests to [Coinmarketcap](https://pro.coinmarketcap.com/api/documentation/v1/#).
The ranking service obtains ranking (24h Full Volume) information from [Cryptocompare](https://min-api.cryptocompare.com/documentation).

Since the update frequency of external information is every 60 seconds for price and every 120 seconds
for volume ranking, data cannot be obtained with time resolution better than one minute. The history service
uses this fact to minimize the computational cost and number of calls to external services required to keep track of
historical records. A cron-like, pure-Python scheduler launches a push job every 60 seconds that performs requests on the pricing 
and ranking services, integrates the returned data and inserts the set of records into the historical database along with a
UTC timestamp. 

Timestamps are used by the endpoint to retrieve historical records from this database. Internally, timestamps are always
truncated to one-minute resolution (seconds and microseconds are discarded) for database and cache lookups.
Since sets of cryptocurrency records are always manipulated in single-timestamp blocks, the application database 
is a MongoDB document database.

If a valid timestamp is provided by the client, the endpoint will perform a cache (Redis) lookup instead of
requesting fresh data from the integration service. If there are no cached results for the requested timestamp,
it will then perform a lookup in the historical database. Both retrieving fresh cryptocurrency records from the integration service
or historical records from the database result in a caching operation.

The limit parameter is only applied at the endpoint just before returning results. While it can be propagated
to the microservices in the backend, it is never propagated to the external APIs. The reason for this is that retrieval
of ranking information is limited to a maximum of 100 items, which implies that each batch of fresh data may contain a maximum of
100 cryptocurrency symbols for which both pricing and ranking information are available. Records with either value missing
are discarded. Since both values can easily change across requests, not placing a limit on pricing requests should maximize the
number of cryptocurrency symbols with both price and ranking information that can be returned or stored in the historical
database. Besides, caching operations are currently limit-unaware.

All communications between the endpoint and the backend (RabbitMQ, Redis, MongoDB) are asynchronous.







