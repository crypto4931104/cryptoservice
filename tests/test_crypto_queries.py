import pytest

import crypto_queries


@pytest.mark.describe('Cryptocurrency API queries.')
class TestCryptocurrencyQueries:

    @pytest.mark.it('Fetches price data.')
    def test_fetch_price(self):
        price_data = crypto_queries.query_coinmarketcap()
        assert isinstance(price_data, list)
        assert all([isinstance(item, dict) for item in price_data])
        assert all(['symbol' in item for item in price_data])
        assert all(['price' in item for item in price_data])

    @pytest.mark.it('Fetches price data with limit.')
    def test_fetch_price_with_limit(self):
        limit = 4
        price_data = crypto_queries.query_coinmarketcap(limit=limit)
        assert len(price_data) == limit

    @pytest.mark.it('Fetches ranking data.')
    def test_fetch_rank(self):
        ranking_data = crypto_queries.query_cryptocompare()
        assert isinstance(ranking_data, dict)
        assert all([type(key) is str for key in ranking_data.keys()])
        assert all([type(value) is int for value in ranking_data.values()])

    @pytest.mark.it('Fetches ranking data with limit.')
    def test_fetch_rank_with_limit(self):
        limit = 4
        ranking_data = crypto_queries.query_cryptocompare(limit=limit)
        assert len(ranking_data) == limit

    @pytest.mark.it('Fetches integrated price and ranking data.')
    def test_fetch_integrated_price_rank(self):
        integrated_data = crypto_queries.fetch_price_ranking()
        assert isinstance(integrated_data, list)
        assert all([isinstance(item, dict) for item in integrated_data])
        assert all(['symbol' in item for item in integrated_data])
        assert all(['price' in item for item in integrated_data])
        assert all(['rank' in item for item in integrated_data])
        assert all([item['rank'] is not None for item in integrated_data])

    @pytest.mark.it('Fetches integrated price and ranking data with limit.')
    def test_fetch_integrated_price_rank_with_limit(self):
        limit = 4
        integrated_data = crypto_queries.fetch_price_ranking(limit=limit)
        assert isinstance(integrated_data, list)
        assert len(integrated_data) == limit
