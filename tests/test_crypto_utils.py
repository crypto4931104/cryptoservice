import pytest

import crypto_queries
import crypto_utils


@pytest.mark.describe('Cryptocurrency utils.')
class TestCryptocurrencyUtils:

    @pytest.mark.it('Integrates price and ranking data.')
    def test_integrate_price_rank(self):
        price_data = crypto_queries.query_coinmarketcap()
        ranking_data = crypto_queries.query_cryptocompare()
        integrated_data = crypto_utils.integrate_price_ranking(price_data, ranking_data)
        assert isinstance(integrated_data, list)
        assert all([isinstance(item, dict) for item in integrated_data])
        assert all(['symbol' in item for item in integrated_data])
        assert all(['price' in item for item in integrated_data])
        assert all(['rank' in item for item in integrated_data])
        assert all([item['rank'] is not None for item in integrated_data])

    @pytest.mark.it('Writes csv.')
    def test_write_csv(self):
        separator = ','
        limit = 4
        integrated_data = crypto_queries.fetch_price_ranking(limit=limit)
        # csv-converted data should be returned as a string.
        csv_data = crypto_utils.as_csv(integrated_data)
        assert isinstance(csv_data, str)
        # Output should contain a header row with appropriate column names.
        csv_lines = csv_data.split("\n")
        assert len(csv_lines) == limit + 1
        assert csv_lines[0] == separator.join(["Rank", "Symbol", "Price USD"])
        # All lines should contain the same number of fields.
        row_count_items = [len(line.split(separator)) for line in csv_lines]
        assert all([count == row_count_items[0] for count in row_count_items])
