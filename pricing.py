from fastapi import FastAPI
from fastapi.responses import JSONResponse
from requests import Session

import crypto_queries


session: Session

app = FastAPI()


@app.on_event("startup")
async def startup() -> None:
    """
    Initialize a Coinmarketcap session to be reused across requests.

    :return: None
    """

    global session
    session = crypto_queries.coinmarketcap_session()


@app.get("/")
def prices():
    """
    Retrieve cryptocurrency price (USD) information from the Coinmarketcap API.

    :return: A json response representing a list of cryptocurrency records containing a symbol and a price.
    """
    price_data = crypto_queries.query_coinmarketcap(session=session)
    return JSONResponse(price_data)
