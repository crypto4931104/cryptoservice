import json
import os
import sys

from requests import Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects

import crypto_utils


COINMARKETCAP_URL = os.environ["COINMARKETCAP_URL"]
COINMARKETCAP_KEY = os.environ["COINMARKETCAP_KEY"]
CRYPTOCOMPARE_URL = os.environ["CRYPTOCOMPARE_URL"]
CRYPTOCOMPARE_KEY = os.environ["CRYPTOCOMPARE_KEY"]


def coinmarketcap_session() -> Session:
    """
    Initialize a Coinmarketcap session.

    :return: A Session object to perform requests to the Coinmarketcap API.
    """
    headers = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': COINMARKETCAP_KEY
    }
    session = Session()
    session.headers.update(headers)
    return session


def query_coinmarketcap(limit: int = 1000, session: Session = None) -> list[dict] | None:
    """
    Perform a request of cryptocurrency prices (USD) to Coinmarketcap.

    :param limit: The maximum number of records to retrieve.
    :param session: An optional Session object to be reused across requests. If not provided, each call
    will initialize a fresh session.
    :return: A list of dictionaries containing simplified cryptocurrency items with a symbol and price fields.
    """
    try:
        if session is None:
            session = coinmarketcap_session()
        parameters = {'start': '1', 'limit': limit, 'convert': 'USD'}
        response = session.get(COINMARKETCAP_URL, params=parameters)
        data = json.loads(response.text)
        price_data = [crypto_utils.simplify_coinmarketcap_item(item) for item in data['data']]
        return price_data
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e, file=sys.stderr)
        return None


def cryptocompare_session() -> Session:
    """
        Initialize a Cryptocompare session.

        :return: A Session object to perform requests to the Cryptocompare API.
        """
    headers = {
        'Accepts': 'application/json',
        # CryptoCompareReadAll API key.
        'api_key': CRYPTOCOMPARE_KEY
    }
    session = Session()
    session.headers.update(headers)
    return session


def query_cryptocompare(limit: int = 100, session: Session = None) -> dict | None:
    """
    Perform a request of cryptocurrency rankings (24h Full Volume) to Cryptocompare.

    :param limit: The maximum number of records to retrieve. Should be in the interval [1, 100].
    :param session: An optional Session object to be reused across requests. If not provided, each call
    will initialize a fresh session.
    :return: A dictionary containing cryptocurrency ranking values by symbol.
    """
    try:
        if session is None:
            session = cryptocompare_session()
        parameters = {'limit': limit, 'tsym': 'USD', 'ascending': 'true'}  # limit = [Min - 1][Max - 100]
        response = session.get(CRYPTOCOMPARE_URL, params=parameters)
        data = json.loads(response.text)
        ranking_data = [item['CoinInfo']['Name'] for item in data['Data']]
        ranking_data = {currency: index + 1 for index, currency in enumerate(ranking_data)}
        return ranking_data
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e, file=sys.stderr)
        return None


def fetch_price_ranking(limit: int = 1000) -> list[dict] | None:
    """
    A convenience function to query Coinmarketcap and Cryptocompare sequentially and integrate the results.

    :param limit: The maximum number of records to be returned.
    :return: A list of dictionaries containing cryptocurrency records with a symbol, price and ranking field.
    """
    # Obtain price (USD) data from Coinmarketcap API.
    price_data = query_coinmarketcap()
    # Obtain ranking (24h Full Volume) data from Cryptocompare API.
    ranking_data = query_cryptocompare()

    if price_data is None or ranking_data is None:
        return None
    else:
        integrated_data = crypto_utils.integrate_price_ranking(price_data, ranking_data)
        integrated_data = crypto_utils.apply_limit(integrated_data, limit=limit)
        return integrated_data
