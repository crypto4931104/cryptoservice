from fastapi import FastAPI
from fastapi.responses import JSONResponse
from requests import Session

import crypto_queries


session: Session

app = FastAPI()


@app.on_event("startup")
async def startup() -> None:
    """
    Initialize a Cryptocompare session to be reused across requests.

    :return: None
    """

    global session
    session = crypto_queries.cryptocompare_session()


@app.get("/")
def rankings():
    """
    Retrieve cryptocurrency ranking (24h Full Volume) information from the Cryptocompare API.

    :return: A json response representing a dictionary of cryptocurrency rankings by symbol.
    """
    ranking_data = crypto_queries.query_cryptocompare(session=session)
    return JSONResponse(ranking_data)
