import asyncio
import json
import os
import sys

import requests
from aio_pika import Message, connect
from aio_pika.abc import AbstractIncomingMessage

import crypto_utils


RABBITMQ_URL = os.environ["RABBITMQ_URL"]
RPC_INTEGRATION_QUEUE = os.environ["RPC_INTEGRATION_QUEUE"]
PRICING_SERVICE_URL = os.environ["PRICING_SERVICE_URL"]
RANKING_SERVICE_URL = os.environ["RANKING_SERVICE_URL"]


def fetch_price_ranking(limit: int = 0) -> list[dict]:
    """
    Convenience function to request and integrate data from the application's internal pricing and ranking services.

    :param limit: The maximum number of records to return. If zero, no limit is applied.
    :return: A list of dictionaries representing cryptocurrency records.
    """

    # Obtain price (USD) data from pricing service.
    response_price = requests.get(PRICING_SERVICE_URL)
    # Obtain ranking (24h Full Volume) data from ranking service.
    response_ranking = requests.get(RANKING_SERVICE_URL)

    price_data = json.loads(response_price.text)
    ranking_data = json.loads(response_ranking.text)

    integrated_data = crypto_utils.integrate_price_ranking(price_data, ranking_data)
    integrated_data = crypto_utils.apply_limit(integrated_data, limit=limit)

    return integrated_data


async def main() -> None:
    """
    Serve a set of cryptocurrency records by integrating queries to the internal pricing and ranking services
    upon receiving an async RPC call. The call provides a limit parameter indicating the maximum number
    of records to be returned.

    :return: None
    """

    # Establish connection, create a channel and declare a queue with name defined by RPC_INTEGRATION_QUEUE.
    connection = await connect(RABBITMQ_URL)
    channel = await connection.channel()
    exchange = channel.default_exchange
    queue = await channel.declare_queue(RPC_INTEGRATION_QUEUE)

    # Start listening.
    async with queue.iterator() as queue_iterator:
        message: AbstractIncomingMessage
        async for message in queue_iterator:
            try:
                async with message.process(requeue=False):
                    assert message.reply_to is not None

                    # Obtain limit parameter from incoming message.
                    limit = int(message.body.decode('utf-8'))

                    # Integrate cryptocurrency data from pricing and ranking services.
                    integrated_data = fetch_price_ranking(limit=limit)

                    # Send response back to naive_endpoint.
                    response = json.dumps(integrated_data).encode('utf-8')
                    await exchange.publish(
                        Message(
                            body=response,
                            correlation_id=message.correlation_id
                        ),
                        routing_key=message.reply_to
                    )
            except Exception:  # TODO: find a suitable, less generic exception class.
                print(f"Processing error for message {message}", file=sys.stderr)


if __name__ == "__main__":
    asyncio.run(main())
