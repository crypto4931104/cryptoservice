import asyncio
import uuid
from aio_pika import Message, connect
from aio_pika.abc import (
    AbstractChannel, AbstractConnection, AbstractIncomingMessage, AbstractQueue
)
from typing import MutableMapping


class IntegrationRpcClient:
    """
    Adapted from an official aio-pika example:
    https://aio-pika.readthedocs.io/en/latest/rabbitmq-tutorial/6-rpc.html
    """

    connection: AbstractConnection
    channel: AbstractChannel
    callback_queue: AbstractQueue

    def __init__(self, rabbitmq_url: str, routing_key: str) -> None:
        self.rabbitmq_url = rabbitmq_url
        self.routing_key = routing_key
        self.futures: MutableMapping[str, asyncio.Future] = {}

    async def connect(self) -> "IntegrationRpcClient":
        self.connection = await connect(self.rabbitmq_url)
        self.channel = await self.connection.channel()
        self.callback_queue = await self.channel.declare_queue(exclusive=True)
        await self.callback_queue.consume(self.on_response, no_ack=True)

        return self

    # The ‘on_response’ callback executed on every response is doing a very simple job, for every
    # response message it checks if the correlation_id is the one we’re looking for.
    # If so, it saves the response in self.response and breaks the consuming loop.
    async def on_response(self, message: AbstractIncomingMessage) -> None:
        if message.correlation_id is None:
            print(f"Processing error for message {message}")
            return

        future: asyncio.Future = self.futures.pop(message.correlation_id)
        future.set_result(message.body)

    # Main call method doing the actual RPC request.
    async def call(self, limit: int = 0) -> str:
        correlation_id = str(uuid.uuid4())
        loop = asyncio.get_running_loop()
        future = loop.create_future()

        self.futures[correlation_id] = future

        await self.channel.default_exchange.publish(
            Message(
                str(limit).encode('utf-8'),
                content_type="text/plain",
                correlation_id=correlation_id,
                reply_to=self.callback_queue.name
            ),
            routing_key=self.routing_key
        )

        return (await future).decode('utf-8')
