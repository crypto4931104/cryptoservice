import json
import datetime
import os
import sys

import requests
from pymongo import MongoClient
from urllib.parse import quote_plus

import crypto_utils


PRICING_SERVICE_URL = os.environ["PRICING_SERVICE_URL"]
RANKING_SERVICE_URL = os.environ["RANKING_SERVICE_URL"]

MONGODB_HOST = os.environ["MONGODB_HOST"]
MONGODB_USER = os.environ["MONGODB_USER"]
MONGODB_PWD = os.environ["MONGODB_PWD"]
MONGODB_DBNAME = os.environ["MONGODB_DBNAME"]
MONGODB_COLLECTION = os.environ["MONGODB_COLLECTION"]

MONGODB_URI = f"mongodb://{quote_plus(MONGODB_USER)}:{quote_plus(MONGODB_PWD)}@{MONGODB_HOST}/{MONGODB_DBNAME}?authSource=admin"


def fetch_price_ranking(limit: int = 0) -> list[dict]:
    """
    Convenience function to request and integrate data from the application's internal pricing and ranking services.

    :param limit: The maximum number of records to return. If zero, no limit is applied.
    :return: A list of dictionaries representing cryptocurrency records.
    """

    # Obtain price (USD) data from pricing service.
    response_price = requests.get(PRICING_SERVICE_URL)
    # Obtain ranking (24h Full Volume) data from ranking service.
    response_ranking = requests.get(RANKING_SERVICE_URL)

    price_data = json.loads(response_price.text)
    ranking_data = json.loads(response_ranking.text)

    integrated_data = crypto_utils.integrate_price_ranking(price_data, ranking_data)
    integrated_data = crypto_utils.apply_limit(integrated_data, limit=limit)

    return integrated_data


def push_to_mongodb() -> None:
    """
    Establish a connection with the application's database service (MongoDB), request and integrate
    fresh cryptocurrency data from the application's internal pricing and ranking services, and
    store it in the database indexed using a UTC timestamp with one-minute resolution.

    :return: None.
    """

    # Request and integrate pricing and ranking data (as many as available).
    try:
        data = fetch_price_ranking(limit=0)
    except Exception as e:
        print(e, file=sys.stderr)
        return None

    # Get UTC timestamp with one-minute resolution for database queries.
    timestamp = datetime.datetime.now(tz=datetime.UTC).replace(second=0, microsecond=0)

    # Establish connection to mongodb.
    mongo_client = MongoClient(MONGODB_URI)
    db = mongo_client[MONGODB_DBNAME]
    historical_collection = db[MONGODB_COLLECTION]

    # Push cryptocurrency data array indexed by timestamp.
    historical_collection.insert_one({'timestamp': timestamp, 'data': data})

    mongo_client.close()


if __name__ == "__main__":
    push_to_mongodb()
